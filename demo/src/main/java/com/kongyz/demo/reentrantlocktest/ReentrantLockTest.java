package com.kongyz.demo.reentrantlocktest;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 测试可重复锁
 */
public class ReentrantLockTest {

    public static void main(String[] args) {
        ReentrantLock lock=new ReentrantLock();
        
        for (int i=1;i<=3;i++){
            System.out.println("准备获取锁["+i+"]");
            lock.lock();
            System.out.println("获取到锁["+i+"]");
        }
        // {@Link ReentrantLock} 是可重入锁，在前面的获取锁执行完毕后，才相继执行后面的释放锁
        for (int i=1;i<=3;i++){
            try {
                System.out.println("准备释放锁["+i+"]");    
            }finally {
                lock.unlock();
                System.out.println("释放锁["+i+"]");
            }
        }
        
    }
}
