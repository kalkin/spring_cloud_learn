package com.kongyz.demo.threadtest;

import java.text.SimpleDateFormat;
import java.util.Random;

/**
 */
public class ThreadLocalExample implements Runnable {
    private static final ThreadLocal<SimpleDateFormat> formatter=ThreadLocal.withInitial(()->new SimpleDateFormat("yyyyMMdd HHmm"));
    
    @Override
    public void run() {
        System.out.println("线程名称="+Thread.currentThread().getName()+"，默认 formatter="+formatter.get().toPattern());
        try {
            Thread.sleep(new Random().nextInt(1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        formatter.set(new SimpleDateFormat());

        System.out.println("线程名称="+Thread.currentThread().getName()+"，修改后 formatter="+formatter.get().toPattern());
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadLocalExample obj=new ThreadLocalExample();
        for (int i=0;i<10;i++){
            Thread t=new Thread(obj,""+i);
            Thread.sleep(new Random().nextInt(1000));
            t.start();
        }
    }
    
}
