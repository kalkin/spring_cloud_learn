package com.kongyz.demo.threadtest;

/**
 * @Description:
 * @Author Kongyz on 2019-10-21 10:16.
 */
public class DeadLockDemo {
    //1号资源
    private static Object resource1=new Object();
    //2号资源
    private static Object resource2=new Object();

    public static void main(String[] args) {
        new Thread(()->{
            synchronized (resource1){
                System.out.println(Thread.currentThread()+"获取资源1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread()+"等待获取资源2");
                synchronized (resource2){
                    System.out.println(Thread.currentThread()+"获取资源2");
                }
            }
        },"线程1").start();
        new Thread(()->{
            synchronized (resource2){
                System.out.println(Thread.currentThread()+"获取资源2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread()+"等待获取资源1");
                synchronized (resource1){
                    System.out.println(Thread.currentThread()+"获取资源1");
                }
            }
        },"线程2").start();
        
        
    }
    
    
}
