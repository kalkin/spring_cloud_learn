package com.kongyz.demo.threadtest;

import java.util.Date;

/**
 * 简单的Runnable类，需要大约5秒钟来执行其任务
 *
 * @author kongyz
 */
public class MyRunnable implements Runnable {

    private String command;

    MyRunnable(String command) {
        this.command = command;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "Start. Time= " + new Date());
        processCommand();
        System.out.println(Thread.currentThread().getName() + "End. Time= " + new Date());
    }

    /**
     * 功能描述: 暂停5秒
     */
    private void processCommand() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return this.command;
    }
}
