package com.kongyz.demo.threadtest;

import java.util.concurrent.Callable;

/**
 * @author kongyz
 */
public class FactorialTask implements Callable<Integer> {

    private int number;

    FactorialTask(int number) {
        this.number = number;
    }

    @Override
    public Integer call() {
        int fact = 1;
        for (int count = number; count > 1; count--) {
            fact = fact * count;
        }
        return fact;
    }
}
