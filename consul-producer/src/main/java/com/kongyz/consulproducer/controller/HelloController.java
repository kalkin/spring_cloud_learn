package com.kongyz.consulproducer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 */
@RestController
public class HelloController {
    
    @Value("${server.port}")
    private int port;
    
    @RequestMapping("/consul/hello")
    public String hello(){
        return "hello，来着consul的服务  "+port;
//        return "hello，来着consul的服务  2 ";
    }
}
