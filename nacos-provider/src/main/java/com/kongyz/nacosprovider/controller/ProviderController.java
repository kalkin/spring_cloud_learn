package com.kongyz.nacosprovider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kongyz
 */
@RestController
@RefreshScope
public class ProviderController {
    @Value("${server.port}")
    private int port;


    @RequestMapping("/hi")
    public String hi(@RequestParam(value = "name", defaultValue = "测试", required = false) String name) {
//        return "来自服务提供者"+port+"：hi："+name;
        return "来自服务提供者：hi：" + name;
    }
}
