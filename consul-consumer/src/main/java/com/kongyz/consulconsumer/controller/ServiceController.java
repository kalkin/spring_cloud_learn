package com.kongyz.consulconsumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 */
@RestController
public class ServiceController {
    
    @Autowired
    private LoadBalancerClient loadBalancerClient;
    @Autowired
    private DiscoveryClient discoveryClient;

    /**
     * 功能描述: 获取所有服务实例
     */
    @RequestMapping("/services")
    public Object services(){
        return discoveryClient.getInstances("consul-service-producer");
    }

    /**
     * 功能描述: 从所有服务中选择一个服务（轮询）
     */
    @RequestMapping("/discover")
    public Object discover(){
        return loadBalancerClient.choose("consul-service-producer").getUri().toString();
    }
    
    
    
    
    
}
