package com.kongyz.eurekaconsumer.hello.hystrix;

import com.kongyz.eurekaconsumer.hello.remote.HelloRemote;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.eurekaconsumer.hello.hystrix
 * @Company 
 * @Description:
 * @Author Kongyz on 2019-09-19 13:46.
 */
@Component
public class HelloRemoteHystrix implements HelloRemote {
    @Override
    public String hello(@RequestParam("name") String name) {
        return "Hello:"+name+"，调用远程服务失败了，这是调用的本地服务";
    }
}
