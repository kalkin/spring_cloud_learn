package com.kongyz.eurekaconsumer.hello.remote;

import com.kongyz.eurekaconsumer.hello.hystrix.HelloRemoteHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.eurekaconsumer.hello
 * @Company 
 * @Description:
 * @Author Kongyz on 2019-09-19 10:07.
 */
@FeignClient(name = "eureka-producer",fallback = HelloRemoteHystrix.class)
public interface HelloRemote {
    
    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name") String name);
}
