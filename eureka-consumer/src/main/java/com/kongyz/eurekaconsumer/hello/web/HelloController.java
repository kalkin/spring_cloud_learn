package com.kongyz.eurekaconsumer.hello.web;

import com.kongyz.eurekaconsumer.hello.remote.HelloRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.eurekaconsumer.hello.web
 * @Company 
 * @Description:
 * @Author Kongyz on 2019-09-19 10:10.
 */
@RestController
public class HelloController {
    
    @Autowired
    private HelloRemote helloRemote;
    
    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable(name = "name")String name){
        
        return helloRemote.hello(name)+"；此消息来自 Feign consumer";
    }
    
}
