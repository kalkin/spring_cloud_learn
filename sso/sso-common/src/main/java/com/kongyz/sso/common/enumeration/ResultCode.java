package com.kongyz.sso.common.enumeration;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.sso.common.enumeration
 * @Description: 响应状态码
 * @Author Kongyz on 2019-11-23 14:38.
 */
public enum  ResultCode {

    //处理成功
    SUCCESS(200, "Successful"),

    //处理失败
    FAILED(500, "Failed"),

    //未登录
    NOT_LOGIN(401, "not login"),

    //未激活
    NOT_ACTIVED(402, "account not actived"),

    //访问拒绝
    ACCESS_DENIED(403, "Access denied"),

    //数据库错误
    DB_ERROR(503, "Error querying database"),

    //参数错误
    PARAM_PARAMETER_ERROR(501, "Parameter error"),

    //参数为空
    PARAM_PARAMETER_IS_NULL(502, "Parameter is null"),
    ;
    
    private Integer code;
    private String msg;

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    
}
