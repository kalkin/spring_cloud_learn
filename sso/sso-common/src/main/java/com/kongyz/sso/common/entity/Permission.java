package com.kongyz.sso.common.entity;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.sso.common.entity
 * @Description: 权限表
 * @Author Kongyz on 2019-11-23 14:11.
 */
@Data
public class Permission {
    
    private Long id;
    private String zuulPrefix;
    private String servicePrefix;
    private String method;
    private String uri;
    private Timestamp createTime;
    private Timestamp updateTime;
}
