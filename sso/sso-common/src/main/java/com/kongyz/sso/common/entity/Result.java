package com.kongyz.sso.common.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.sso.common.entity
 * @Description:  返回实体
 * @Author Kongyz on 2019-11-23 14:18.
 */
@Data
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public class Result {
    
    private Integer code;
    private String msg;
    private Object data;
}
