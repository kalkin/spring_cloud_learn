package com.kongyz.sso.common.entity;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Set;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.sso.common.entity
 * @Description: 用户
 * @Author Kongyz on 2019-11-23 13:12.
 */
@Data
public class Member {
    
    private Integer id;
    private String memberName;
    private String password;
    private String mobile;
    private String email;
    private short sex;
    private Timestamp birthday;
    private Timestamp createTime;
    private Set<Role> roles;
            
}
