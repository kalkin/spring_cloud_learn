package com.kongyz.sso.common.entity;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Set;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.sso.common.entity
 * @Description: 角色
 * @Author Kongyz on 2019-11-23 14:04.
 */
@Data
public class Role {
    
    private Long id;
    private String roleName;
    private short valid;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Set<Permission> permissions;
}
