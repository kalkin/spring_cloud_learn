package com.kongyz.ribbonservice.hello.controller;

import com.kongyz.ribbonservice.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.ribbonservice.hello.controller
 * @Company 
 * @Description:
 * @Author Kongyz on 2019-09-24 15:43.
 */
@RestController
public class HelloController {
    @Autowired
    private HelloService helloService;
    
    @RequestMapping("/hi")
    public String hi(@RequestParam String name){
        return helloService.hello(name);
    }
    
}
