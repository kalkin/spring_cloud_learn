package com.kongyz.ribbonservice.hello.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.ribbonservice.hello.service
 * @Company 
 * @Description:
 * @Author Kongyz on 2019-09-19 15:42.
 */
@Service
public class HelloService {
    
    @Autowired
    private RestTemplate restTemplate;
    
    public String hello(String name){
        return restTemplate.getForObject("http://EUREKA-PRODUCER/hello?name="+name, String.class)+"；此消息来自Ribbon";
    }
}
