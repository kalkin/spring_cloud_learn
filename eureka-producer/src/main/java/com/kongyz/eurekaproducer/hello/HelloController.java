package com.kongyz.eurekaproducer.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project : springcloudlearn
 * @PackageName : com.kongyz.eurekaproducer.hello
 * @Company 
 * @Description:
 * @Author Kongyz on 2019-09-19 9:32.
 */
@RestController
public class HelloController {
    
    @RequestMapping("/hello")
    public String index(@RequestParam String name){
        return "Hello："+ name+",这条消息来自远程服务提供者！";
    }
    
}
